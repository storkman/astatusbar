#define _DEFAULT_SOURCE
#include <errno.h>
#include <stdarg.h>
#include <stdio.h>
#include <time.h>

#include "common.h"

#define FULLSEC 1000000000

const char *format = "[%H:%M:%S %a %d-%m-%Y]";

int
main(int argc, char *argv[])
{
	struct timespec rt, mt;
	time_t last = 0;
	int err;
	char buf[128];
	for (;;) {
		if (clock_gettime(CLOCK_MONOTONIC, &mt))
			die("can't read clock:");
		if (clock_gettime(CLOCK_REALTIME, &rt))
			die("can't read clock:");
		
		if (rt.tv_sec != last) {
			strftime(buf, sizeof(buf), format, localtime(&rt.tv_sec));
			printf("%s\n", buf);
			fflush(stdout);
			last = rt.tv_sec;
		}
		mt.tv_nsec += FULLSEC - rt.tv_nsec;
		mt.tv_sec += mt.tv_nsec / FULLSEC;
		mt.tv_nsec %= FULLSEC;
		while ((err = clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME, &mt, NULL))) {
			if (err != EINTR)
				die("can't sleep:");
		}
	}		
}
